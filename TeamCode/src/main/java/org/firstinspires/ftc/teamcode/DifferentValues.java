package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by Cannon on 10/9/2017.
 */
@TeleOp(name = "the right stuff with arm")

public class DifferentValues extends LinearOpMode {


    private static DcMotor leftFrontWheel, leftBackWheel, rightFrontWheel, rightBackWheel;
    private static Servo leftArm, rightArm, upServo;


    @Override
    public void runOpMode() {
        leftFrontWheel = hardwareMap.dcMotor.get("fl");
        leftBackWheel = hardwareMap.dcMotor.get("bl");
        rightFrontWheel = hardwareMap.dcMotor.get("fr");
        rightBackWheel = hardwareMap.dcMotor.get("br");
        leftArm = hardwareMap.servo.get("leftArm");
        rightArm = hardwareMap.servo.get("rightArm");
        upServo = hardwareMap.servo.get("upServo");
        double threshhold = .2;

        telemetry.addData("Status", "Initialized");
        telemetry.update();
        // Wait for the game to start (driver presses PLAY)
        waitForStart();
        double leftArmVar = 0.30;
        double rightArmVar = 0.70;
        double upServoVar = 0.0;
        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {


            if(Math.abs(gamepad1.left_stick_y)>threshhold)
            {
                leftFrontWheel.setPower((gamepad1.left_stick_y)/2);
                rightFrontWheel.setPower((gamepad1.left_stick_y)/2);
                leftBackWheel.setPower((-gamepad1.left_stick_y)/2);
                rightBackWheel.setPower((gamepad1.left_stick_y)/2);
            }
            else if(Math.abs(gamepad1.left_stick_x)>threshhold)
            {
                leftFrontWheel.setPower((-gamepad1.left_stick_x)/2);
                rightFrontWheel.setPower((gamepad1.left_stick_x)/2);
                leftBackWheel.setPower((-gamepad1.left_stick_x)/2);
                rightBackWheel.setPower((-gamepad1.left_stick_x)/2);
            }

            else if(Math.abs(gamepad1.right_stick_x)>threshhold)
            {
                leftFrontWheel.setPower((-gamepad1.right_stick_x)/2);
                rightFrontWheel.setPower((gamepad1.right_stick_x)/2);
                leftBackWheel.setPower((gamepad1.right_stick_x)/2);
                rightBackWheel.setPower((gamepad1.right_stick_x)/2);
            }
            else
            {
                leftFrontWheel.setPower(0);
                rightFrontWheel.setPower(0);
                leftBackWheel.setPower(0);
                rightBackWheel.setPower(0);
            }
            if(gamepad1.dpad_left)
            {

                leftArmVar = 0.30;
                rightArmVar = 0.70;
                telemetry.addData("Arms", "left");

            }
            else if(gamepad1.dpad_right)
            {

                leftArmVar = 0.70;
                rightArmVar = 0.00;
                telemetry.addData("Arms", "right");

            }
            else if(gamepad1.a)
            {
                leftArmVar = 0.50;
                rightArmVar = 0.50;
                telemetry.addData("Arms", "center");
            }
            else if(gamepad1.y)
            {
                leftArmVar = 0.56;
                rightArmVar = 0.31;
            }
            if(gamepad1.dpad_up)
            {
                upServo.setPosition(1);
            }
            else if(gamepad1.dpad_down)
            {
                upServo.setPosition(0);
            }
            else
            {
                upServo.setPosition(0.5);
            }
            leftArm.setPosition(leftArmVar);
            rightArm.setPosition(rightArmVar);

            telemetry.addData("Status", "Running");
            telemetry.update();

        }
    }

}
