
package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Gyroscope;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;

import static com.sun.tools.javac.util.Constants.format;


@Autonomous(name="Mecanum Auto", group="Test Auto")
//@Disabled
public class MecanumAuto extends LinearOpMode {
    // Declare OpMode members.
    private ElapsedTime runtime = new ElapsedTime();
    private static DcMotor leftFrontWheel, leftBackWheel, rightFrontWheel, rightBackWheel;
    //ColorSensor color_sensor;
    //private Gyroscope imu;

    /* The following refers to the camera and visual recognition system.**/

    public static final String TAG = "Vuforia VuMark Sample";
    OpenGLMatrix lastLocation = null;

    /*
     * {@link #vuforia} is the variable we will use to store our instance of the Vuforia
     * localization engine.
     */
    private VuforiaLocalizer vuforia;


    /**
     * Let's chart out the procedure so we know what we're doing.
     *
     * Going off of the animation video, we need to:
     * 1. Knock off opposing color's jewel
     *      a. Bot must scan surroudings to know its team, and knock off non-matching color
     * 2. Score glyphs in cryptobox key (specific column which can be decoded from pictograph)
     *      a. Bot must know different pictographs, find & scan it, and place glyph in key
     * 3. Park in safe zone
     *      a. Bot must recognize taped-off area and move to be inside it
     */

    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");
        telemetry.update();

        // Initialize the hardware variables. Note that the strings used here as parameters
        // to 'get' must correspond to the names assigned during the robot configuration
        // step (using the FTC Robot Controller app on the phone).
        //imu = hardwareMap.get(Gyroscope.class, "imu"); // Initializing Gyro
        leftFrontWheel = hardwareMap.dcMotor.get("fl");
        leftBackWheel = hardwareMap.dcMotor.get("bl");
        rightFrontWheel = hardwareMap.dcMotor.get("fr");
        rightBackWheel = hardwareMap.dcMotor.get("br");
        //color_sensor = hardwareMap.colorSensor.get("color");

        // Wait for the game to start (driver presses PLAY)
        waitForStart();
        runtime.reset();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {

            knockJewel();

            for (int i = 2; i > 0; i--) {
                scoreGlyph(getCryptoboxKey());
            }

            parkInSafeZone();

            // Update telemetry with the new data. Always run at end of loop.
            updateTelem();
        }
    }



    private void updateTelem() {
        // Show the elapsed game time and wheel power.
        telemetry.addData("Status", "Run Time: " + runtime.toString());
        telemetry.addData("Motors", "left wheels (%.2f), right wheels (%.2f)", leftFrontWheel.getPower(), rightFrontWheel.getPower());
        telemetry.update();
    }

    private void forward(int distance, boolean reverse) {
        int leftBackWheelPower;
        int allOthers;

        for (; distance > 0; distance--) {
            if (reverse) {
                leftBackWheelPower = 50;
                allOthers = -50;
            } else {
                leftBackWheelPower = -50;
                allOthers = 50;
            }
            leftFrontWheel.setPower(allOthers);
            leftBackWheel.setPower(leftBackWheelPower);

            rightFrontWheel.setPower(allOthers);
            rightBackWheel.setPower(allOthers);
        }

    }

//    private char getColor()
//    {
//		if(color_sensor.red()>color_sensor.green()&&color_sensor.red()>color_sensor.blue())
//		{
//			return r;
//		}
//		if(color_sensor.green()>color_sensor.red()&&color_sensor.green()>color_sensor.blue())
//		{
//			return g;
//		}
//		if(color_sensor.blue()>color_sensor.green()&&color_sensor.blue()>color_sensor.red())
//		{
//			return b;
//		}
//    }

    private void knockJewel() {

    }

    private RelicRecoveryVuMark getCryptoboxKey() {

        /* SETUP **/

        /*
         * To start up Vuforia, tell it the view that we wish to use for camera monitor (on the RC phone);
         * If no camera monitor is desired, use the parameterless constructor instead (commented out below).
         */
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters(cameraMonitorViewId);

        parameters.vuforiaLicenseKey = "AV4I40v/////AAAAGa+4WxMAYUZBhfrX5b2dIkB731x7U5qNOM17ntgbrrwWUrV7En4+6ZXLFmwhHWDJxnksGlO7ed3XcdUrXT1JlUriJVVnzUAC8qurxvM9cAniTVMYPV8tDhUMOpUINWHWCUPHtny8yI0wSwq+eDQAIsOCB19H3RQHibzUVZIWUwjo1+m0PNIWIjTsW7Qkyp0Z4LHZfnmipBX+n5e3kv4LSFNHbtNotDgWjW8GJ1qZE38rilL7LzmSqZZOevfymWSDcyR0WYqJn1jIgQwMxHeKiv6ErPe9eVbzWGXJdkeLwWpZ9dh0GsJM+oP8x6uDauQIYEwnEqi+mTygJyNysY/c9OnfK+U+YQscGibnyUNR6fmF";

        /* 
         * We also indicate which camera on the RC that we wish to use.
         * Here we chose the back (HiRes) camera (for greater range), but
         * for a competition robot, the front camera might be more convenient.
         */
        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;
        this.vuforia = ClassFactory.createVuforiaLocalizer(parameters);

        /*
         * Load the data set containing the VuMarks for Relic Recovery. There's only one trackable
         * in this data set: all three of the VuMarks in the game were created from this one template,
         * but differ in their instance id information.
         * @see VuMarkInstanceId
         */
        VuforiaTrackables relicTrackables = this.vuforia.loadTrackablesFromAsset("RelicVuMark");
        VuforiaTrackable relicTemplate = relicTrackables.get(0);
        relicTemplate.setName("relicVuMarkTemplate"); // can help in debugging; otherwise not necessary
        relicTrackables.activate();


        /* Recognizing Pictographs **/


        /*
         * See if any of the instances of {@link relicTemplate} are currently visible.
         * {@link RelicRecoveryVuMark} is an enum which can have the following values:
         * UNKNOWN, LEFT, CENTER, and RIGHT. When a VuMark is visible, something other than
         * UNKNOWN will be returned by {@link RelicRecoveryVuMark#from(VuforiaTrackable)}.
         */
        RelicRecoveryVuMark vuMark = RelicRecoveryVuMark.from(relicTemplate);
        while (true) {
            if (vuMark != RelicRecoveryVuMark.UNKNOWN) {
                /* Found an instance of the template. In the actual game, you will probably
                 * loop until this condition occurs, then move on to act accordingly depending
                 * on which VuMark was visible. */
                telemetry.addData("VuMark", "%s visible", vuMark);

                /* For fun, we also exhibit the navigational pose. In the Relic Recovery game,
                 * it is perhaps unlikely that you will actually need to act on this pose information, but
                 * we illustrate it nevertheless, for completeness. */
                OpenGLMatrix pose = ((VuforiaTrackableDefaultListener)relicTemplate.getListener()).getPose();
                telemetry.addData("Pose", format(pose));

                /* We further illustrate how to decompose the pose into useful rotational and
                 * translational components */
                if (pose != null) {
                    VectorF trans = pose.getTranslation();
                    Orientation rot = Orientation.getOrientation(pose, AxesReference.EXTRINSIC, AxesOrder.XYZ, AngleUnit.DEGREES);

                    // Extract the X, Y, and Z components of the offset of the target relative to the robot
                    double tX = trans.get(0);
                    double tY = trans.get(1);
                    double tZ = trans.get(2);

                    // Extract the rotational components of the target relative to the robot
                    double rX = rot.firstAngle;
                    double rY = rot.secondAngle;
                    double rZ = rot.thirdAngle;
                }
                break;
            }
            else {
                telemetry.addData("VuMark", "not visible");
            }

        }
        telemetry.update();
        return vuMark;



    }

    private void scoreGlyph(RelicRecoveryVuMark column) {

    }

    private void parkInSafeZone() {

    }

}
