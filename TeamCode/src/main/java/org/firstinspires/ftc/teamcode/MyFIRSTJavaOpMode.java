package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import java.util.Arrays;
/**
 * Created by Cannon on 9/23/2017.
 */

@TeleOp
    public class MyFIRSTJavaOpMode extends LinearOpMode {
    private static final double TRIGGERTHRESHOLD = .2;
    private static final double ACCEPTINPUTTHRESHOLD = .15;
    private static final double SCALEDPOWER = 1; //Emphasis on current controller reading (vs current motor power) on the drive train

    private static DcMotor leftFrontWheel, leftBackWheel, rightFrontWheel, rightBackWheel;


            //private DigitalChannel digitalTouch;
            //private DistanceSensor sensorColorRange;
            //private Servo servoTest;


        @Override
        public void runOpMode() {
            //Assigning stuff
            leftFrontWheel = hardwareMap.dcMotor.get("fl");
            leftBackWheel = hardwareMap.dcMotor.get("bl");
            rightFrontWheel = hardwareMap.dcMotor.get("fr");
            rightBackWheel = hardwareMap.dcMotor.get("fl");
            leftFrontWheel.setDirection(DcMotorSimple.Direction.REVERSE);
            leftBackWheel.setDirection(DcMotorSimple.Direction.REVERSE);
            double volts = hardwareMap.voltageSensor.get("Motor Controller 1").getVoltage();

            telemetry.addData("Status", "Initialized");
            telemetry.update();
            // Wait for the game to start (driver presses PLAY)
            waitForStart();

            // run until the end of the match (driver presses STOP)

            double tgtPowerY = 0;
            double tgtPowerX = 0;
            //while robot is running
            while (opModeIsActive()) {
                //sets the power of the motor to the joystick direction (- is forward)


                double inputY = Math.abs(gamepad1.left_stick_y) > ACCEPTINPUTTHRESHOLD ? gamepad1.left_stick_y : 0;
                double inputX = Math.abs(gamepad1.left_stick_x) > ACCEPTINPUTTHRESHOLD ? -gamepad1.left_stick_x : 0;
                double inputC = Math.abs(gamepad1.right_stick_x)> ACCEPTINPUTTHRESHOLD ? -gamepad1.right_stick_x: 0;
                arcadeMecanum(inputY, inputX, inputC, leftFrontWheel, rightFrontWheel, leftBackWheel, rightBackWheel);
                telemetry.addData("Status", "Running");
                //sends the added data to the phone
                telemetry.update();
            }}
            // y - forwards
            // x - side
            // c - rotation
            public static void arcadeMecanum(double y, double x, double c, DcMotor leftFront, DcMotor rightFront, DcMotor leftBack, DcMotor rightBack) {
                double leftFrontVal = y + x + c;
                double rightFrontVal = y - x - c;
                double leftBackVal = y - x + c;
                double rightBackVal = y + x - c;

                //Move range to between 0 and +1, if not already
                double[] wheelPowers = {rightFrontVal, leftFrontVal, leftBackVal, rightBackVal};
                Arrays.sort(wheelPowers);
                if (wheelPowers[3] > 1) {
                    leftFrontVal /= wheelPowers[3];
                    rightFrontVal /= wheelPowers[3];
                    leftBackVal /= wheelPowers[3];
                    rightBackVal /= wheelPowers[3];
                }
                double scaledPower = SCALEDPOWER;

                leftFront.setPower(leftFrontVal*scaledPower+leftFront.getPower()*(1-scaledPower));
                rightFront.setPower(rightFrontVal*scaledPower+rightFront.getPower()*(1-scaledPower));
                leftBack.setPower(leftBackVal*scaledPower+leftBack.getPower()*(1-scaledPower));
                rightBack.setPower(rightBackVal*scaledPower+rightBack.getPower()*(1-scaledPower));

            }
        }

